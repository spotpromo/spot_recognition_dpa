package br.com.spotpromo.spotrecognition

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import br.com.spotpromo.spotrecognition.model.DataRecognition
import br.com.spotpromo.spotrecognition.tflite.Classifier
import br.com.spotpromo.spotrecognition.tflite.DetectorFactory
import br.com.spotpromo.spotrecognition.utils.MultBoxTracker
import br.com.spotpromo.spotrecognition.utils.UtilsRI
import com.google.gson.Gson
import org.jetbrains.anko.activityUiThread
import org.jetbrains.anko.doAsync
import java.io.File
import java.io.IOException
import java.util.*


class SpotRecognition :AppCompatActivity(){

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        //setContentView(R.layout.activity_main)
//        initBox(this)
//    }

    var previewWidth = 0
    var previewHeight = 0

    val TF_OD_API_INPUT_SIZE = 928

    private var detector: Classifier? = null

    fun initBox(context: Activity) {
        try {

            val modelStrings = UtilsRI.getModelStrings(
                context.assets,
                UtilsRI.ASSETS_PATH
            )
            Log.e("ANDROID_ASSETS", modelStrings[0])
            if (detector == null)
                detector = DetectorFactory.getDetector(context.assets, modelStrings[0])

        } catch (e: IOException) {
            e.printStackTrace()

            val toast = Toast.makeText(
                context, "Classifier could not be initialized", Toast.LENGTH_SHORT
            )
            toast.show()
        }
    }


    fun onRecognition(context: Activity, path: String): DataRecognition? {

        var result: DataRecognition? = null
//        doAsync {
//
//            activityUiThread {


        try {
            val file = File(path)
            if (file.isFile) {
                val bitmap: Bitmap = BitmapFactory.decodeFile(path)
                previewWidth = bitmap.width
                previewHeight = bitmap.height;
                val cropImage: Bitmap? = UtilsRI.processBitmap(bitmap, TF_OD_API_INPUT_SIZE)

                val recognition = detector!!.recognizeImage(cropImage)
                result = handleResult(context, path, cropImage!!, recognition)
            }

        } catch (err: Exception) {
            err.printStackTrace()
        }
//            }
//        }

        return result
    }

    private fun handleResult(
        context: Activity,
        path: String,
        bitmap: Bitmap,
        results: List<Classifier.Recognition>
    ): DataRecognition? {

        try {
            if (!results.isNullOrEmpty()) {
                val canvas = Canvas(bitmap)

                MultBoxTracker.proccess(
                    context,
                    results,
                    canvas,
                    UtilsRI.getRotationFile(path),
                    bitmap.width,
                    bitmap.height
                )
                val resizeBitmap = UtilsRI.getReszeBitmap(
                    context,
                    bitmap,
                    previewWidth,
                    previewHeight
                )
                val data = DataRecognition()
                data.path = resizeBitmap
                data.recognition = MultBoxTracker.getDataTrackedObjects()
                return data
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

        return null

    }


}