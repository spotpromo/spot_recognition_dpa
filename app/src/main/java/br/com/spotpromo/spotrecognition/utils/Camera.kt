package br.com.spotpromo.spotrecognition.utils

import android.app.Activity
import android.content.Intent
import android.util.Log

object Camera {

    fun camera(context: Activity, type: Int) {

        try {
//            context.startActivityForResult(
//                Intent(context, ActivityCameraPreview::class.java).putExtra("tipo", type), type
//            )

        } catch (e: Exception) {
            Log.v("TAG", "Não é possível selecionar a foto.")
        }
    }

    fun galeria(context: Activity, type: Int) {

        try {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            context.startActivityForResult(
                Intent.createChooser(intent, "Select the photo"),
                type
            )
        } catch (e: Exception) {
            Log.v("TAG", "Não é possível selecionar a foto.")
        }
    }

}