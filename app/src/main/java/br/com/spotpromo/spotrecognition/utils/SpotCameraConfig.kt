package br.com.spotpromo.spotrecognition.utils

object SpotCameraConfig {

    var buttonColor = "#263552"
    var photoSize = "1024x768"
    var tempFilePath = "temp_file.jpeg"
    var isWithGallery = true
}