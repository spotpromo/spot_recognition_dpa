package br.com.spotpromo.spotrecognition.utils

import android.content.Context
import android.content.res.AssetManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Matrix
import android.media.ExifInterface
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import java.io.*
import java.nio.MappedByteBuffer
import java.nio.channels.FileChannel
import java.util.*
import android.os.Environment
import android.util.Log
import java.text.SimpleDateFormat


object UtilsRI {
    var ASSETS_PATH = ""
    fun getModelStrings(mgr: AssetManager, path: String?): ArrayList<String> {
        val res = ArrayList<String>()
        try {
            val files = mgr.list(path!!)
            for (file in files!!) {
                val splits = file.split("\\.".toRegex()).toTypedArray()
                if (splits[splits.size - 1] == "tflite") {
                    res.add(file)
                }
            }
        } catch (e: IOException) {
            System.err.println("getModelStrings: " + e.message)
        }

        return res
    }

    /**
     * Memory-map the model file in Assets.
     */
    @Throws(IOException::class)
    fun loadModelFile(assets: AssetManager, modelFilename: String): MappedByteBuffer {
        val fileDescriptor = assets.openFd(modelFilename)
        val inputStream = FileInputStream(fileDescriptor.fileDescriptor)
        val fileChannel = inputStream.channel
        val startOffset = fileDescriptor.startOffset
        val declaredLength = fileDescriptor.declaredLength

        Log.e("TENSORFLOWWWW", "------------------------------> MODEL  " + fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength));
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength)
    }

    fun processBitmap(source: Bitmap, size: Int): Bitmap? {
        val image_height = source.height
        val image_width = source.width
        val croppedBitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888)
        val frameToCropTransformations = getTransformationMatrix(image_width, image_height, size, size, 0, false)
        val cropToFrameTransformations = Matrix()
        frameToCropTransformations!!.invert(cropToFrameTransformations)
        val canvas = Canvas(croppedBitmap)
        canvas.drawBitmap(source, frameToCropTransformations, null)


        return croppedBitmap
    }

    fun getTransformationMatrix(
        srcWidth: Int,
        srcHeight: Int,
        dstWidth: Int,
        dstHeight: Int,
        applyRotation: Int,
        maintainAspectRatio: Boolean
    ): Matrix? {
        val matrix = Matrix()
        if (applyRotation != 0) {
            // Translate so center of image is at origin.
            matrix.postTranslate(-srcWidth / 2.0f, -srcHeight / 2.0f)

            // Rotate around origin.
            matrix.postRotate(applyRotation.toFloat())
        }

        // Account for the already applied rotation, if any, and then determine how
        // much scaling is needed for each axis.
        val transpose = (Math.abs(applyRotation) + 90) % 180 == 0
        val inWidth = if (transpose) srcHeight else srcWidth
        val inHeight = if (transpose) srcWidth else srcHeight

        // Apply scaling if necessary.
        if (inWidth != dstWidth || inHeight != dstHeight) {
            val scaleFactorX = dstWidth / inWidth.toFloat()
            val scaleFactorY = dstHeight / inHeight.toFloat()
            if (maintainAspectRatio) {
                // Scale by minimum factor so that dst is filled completely while
                // maintaining the aspect ratio. Some image may fall off the edge.
                val scaleFactor = Math.max(scaleFactorX, scaleFactorY)
                matrix.postScale(scaleFactor, scaleFactor)
            } else {
                // Scale exactly to fill dst from src.
                matrix.postScale(scaleFactorX, scaleFactorY)
            }
        }
        if (applyRotation != 0) {
            // Translate back from origin centered reference to destination frame.
            matrix.postTranslate(dstWidth / 2.0f, dstHeight / 2.0f)
        }
        return matrix
    }


    fun getRotationFile(path: String) : Int {
        var rotate = 0
        try {
            val file = File(path)
            if(file.isFile) {
                val exif = ExifInterface(file.absolutePath)
                val orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
                rotate = when(orientation) {
                    ExifInterface.ORIENTATION_ROTATE_270 -> 270
                    ExifInterface.ORIENTATION_ROTATE_180 -> 180
                    ExifInterface.ORIENTATION_ROTATE_90 -> 90
                    else -> 0
                }


            }
        }catch (e: Exception) {
            e.printStackTrace()
        }

        return rotate
    }

    fun getReszeBitmap(context: Context, bm: Bitmap, width: Int, height: Int) : String {
        var bmWidth = bm.width
        var bmHeight = bm.height

        val scaleWidth = width.toFloat() / bmWidth
        val scaleHeight = height.toFloat() / bmHeight

        val croppedBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val frameToCropTransformations: Matrix =
            getTransformationMatrix(
                bmWidth,
                bmHeight,
                width,
                height,
                0,
                false
            )!!
        val cropToFrameTransformations = Matrix()
        frameToCropTransformations.invert(cropToFrameTransformations)

        val canvas = Canvas(croppedBitmap)
        canvas.drawBitmap(bm, frameToCropTransformations, null)

        val out = ByteArrayOutputStream()
        croppedBitmap.compress(Bitmap.CompressFormat.PNG, 100, out)
        val decoded = BitmapFactory.decodeStream(ByteArrayInputStream(out.toByteArray()))

        return saveInStorage(context, decoded)
    }

    fun saveInStorage(context: Context, bm: Bitmap) : String {

        val sdcard = context.getExternalFilesDir(null)

        /** PASTA PROJETO  */
        val pasta_projeto = File(
            sdcard!!.absolutePath + File.separator + "DPA"
        )
        if (!pasta_projeto.isDirectory) pasta_projeto.mkdirs()
        /** PASTA SPOT FOTOS  */
        val pasta_spot = File(
            pasta_projeto.absolutePath + File.separator + "SPOTPROMO"
        )
        if (!pasta_spot.isDirectory) pasta_spot.mkdirs()

        val string_file_copy: String = CriarFileTemporarioString(
            pasta_spot.absolutePath,
            "temp_recognition"
        )

        val file_copy = File(string_file_copy)
        val out = FileOutputStream(file_copy)
        bm.compress(Bitmap.CompressFormat.PNG, 100, out)

        return file_copy.absolutePath

    }

    fun CriarFileTemporarioString(
        diretorio: String,
        nomefoto: String
    ): String {
        val timeStamp =
            SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault())
                .format(Date())
        val mediaFile: File
        mediaFile =
            File(diretorio + File.separator + nomefoto + "_" + timeStamp + ".jpg")
        return mediaFile.absolutePath
    }


    fun View.isvisible(visible: Boolean) {
        visibility = if(visible) View.VISIBLE else View.GONE
    }

    fun View.getvisible() : Boolean = visibility == View.VISIBLE
    fun ImageView.drawable(context: Context, drawable: Int)  = setImageDrawable( ContextCompat.getDrawable(
        context,
        drawable
    ))

}