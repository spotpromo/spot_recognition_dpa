package br.com.spotpromo.spotrecognition.utils

import android.content.Context
import android.graphics.*
import android.text.TextUtils
import android.util.Pair
import android.util.TypedValue
import br.com.spotpromo.spotrecognition.model.BorderedText
import br.com.spotpromo.spotrecognition.tflite.Classifier
import java.lang.StringBuilder

object MultBoxTracker {
    val trackedObjects: ArrayList<TrackedRecognition> = ArrayList<TrackedRecognition>()
    var screenRects: ArrayList<Pair<Float, RectF>> = ArrayList<Pair<Float, RectF>>()
    private var frameToCanvasMatrix: Matrix? = null
    private const val TEXT_SIZE_DIP = 10f
    private const val MIN_SIZE = 16.0f
    private var borderedText: BorderedText? = null
    private val COLORS = intArrayOf(
        Color.BLUE,
        Color.RED,
        Color.GREEN,
        Color.YELLOW,
        Color.CYAN,
        Color.MAGENTA,
        Color.DKGRAY,
        Color.GRAY,
        Color.LTGRAY,
        Color.parseColor("#55FF55"),
        Color.parseColor("#FFA500"),
        Color.parseColor("#FF8888"),
        Color.parseColor("#AAAAFF"),
        Color.parseColor("#FFFFAA"),
        Color.parseColor("#55AAAA"),
        Color.parseColor("#AA33AA"),
        Color.parseColor("#0D0068"),
        Color.parseColor("#F08080"),
        Color.parseColor("#FFBF00"),
        Color.parseColor("#CCCCFF"),
        Color.parseColor("#40E0D0"),
        Color.parseColor("#008080"),
        Color.parseColor("#000080"),
        Color.parseColor("#800080"),
        Color.parseColor("#808000"),
        Color.parseColor("#800000"),
        Color.parseColor("#CD5C5C"),
        Color.parseColor("#0D0068")
    )

    fun proccess(
        context: Context,
        results: List<Classifier.Recognition>,
        canvas: Canvas,
        orientation: Int,
        width: Int,
        height: Int
    ) {
        val textSizePx: Float = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            TEXT_SIZE_DIP,
            context.resources.displayMetrics
        )
        borderedText = BorderedText(textSizePx)

        val rotated = orientation % 180 == 90
        val multiplier = Math.min(
            (canvas.height / (if (rotated) width else height).toFloat()),
            (canvas.width / (if (rotated) height else width).toFloat())
        )

        frameToCanvasMatrix = UtilsRI.getTransformationMatrix(
            width,
            height,
            (multiplier * if (rotated) height else width).toInt(),
            (multiplier * if (rotated) width else height).toInt(),
            orientation,
            false
        )

        results(results)

        if (!trackedObjects.isNullOrEmpty())
            draw(trackedObjects, canvas)

    }

    fun draw(
        tracked: ArrayList<TrackedRecognition>,
        canvas: Canvas
    ) {

        for (recognition: TrackedRecognition in tracked) {
            val trackedPos = RectF(recognition.location)
            getFrameToCanvasMatrix().mapRect(trackedPos)
            val boxPaint = Paint()
            boxPaint.style = Paint.Style.STROKE
            boxPaint.strokeWidth = 1.0f
            boxPaint.color = recognition.color

            val cornerSize = Math.min(trackedPos.width(), trackedPos.height()) / 8.0f
            canvas.drawRoundRect(trackedPos, cornerSize, cornerSize, boxPaint)
            val labelString: String = if (!TextUtils.isEmpty(recognition.title)) String.format(
                "%s %.2f",
                recognition.title,
                (100 * recognition.detectionConfidence)
            ) else
                String.format("%.2f", (100 * recognition.detectionConfidence))

            borderedText!!.drawText(
                canvas, trackedPos.left + cornerSize, trackedPos.top, "$labelString%", boxPaint
            )
        }


    }

    private fun results(results: List<Classifier.Recognition>): ArrayList<TrackedRecognition> {

        val rectsToTrack: ArrayList<Pair<Float, Classifier.Recognition>> =
            ArrayList<Pair<Float, Classifier.Recognition>>()
        val rgbFrameToScreen = Matrix(getFrameToCanvasMatrix())
        screenRects.clear()
        for (result in results) {
            if (result.location == null)
                continue
            val detectionFrameRect = RectF(result.location)
            val detectionScreenRect = RectF()
            rgbFrameToScreen.mapRect(detectionScreenRect, detectionFrameRect)

            screenRects.add(Pair<Float, RectF>(result.confidence, detectionScreenRect))

            if (detectionFrameRect.width() < MIN_SIZE || detectionFrameRect.height() < MIN_SIZE)
                continue

            rectsToTrack.add(Pair<Float, Classifier.Recognition>(result.confidence, result))
        }

        trackedObjects.clear()

        for (potential in rectsToTrack) {
            val trackedRecognition = TrackedRecognition()
            trackedRecognition.detectionConfidence = potential.first
            trackedRecognition.location = RectF(potential.second.location)
            trackedRecognition.title = potential.second.title
            trackedRecognition.color = COLORS[potential.second.detectedClass % COLORS.size]
            trackedObjects.add(trackedRecognition)
        }

        return trackedObjects
    }

    fun getDataTrackedObjects(): ArrayList<TrackedRecognition> = trackedObjects

    private fun getFrameToCanvasMatrix(): Matrix = frameToCanvasMatrix!!

    class TrackedRecognition {
        var location: RectF? = null
        var detectionConfidence = 0f
        var color = 0
        var title: String? = null
        var quantidade: Int? = null
        var listRects: ArrayList<RectF>? = null

        fun getText() : String {
            val sbHtml = StringBuilder()
            sbHtml.append("<font color='$color'>Titulo: $title </font>")
            sbHtml.append("<br/>Detecção: ${String.format("%.2f", (100 * detectionConfidence))}")
            return sbHtml.toString()
        }
    }



}